extern crate pbr;

use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io;
use std::fs::OpenOptions;
use pbr::{ProgressBar, Units};

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let mut inf = File::open(&args[1])?;
    let mut outf = OpenOptions::new()
               .read(true)
               .write(true)
               .open(&args[2])?;
    let size = inf.seek(io::SeekFrom::End(0))?;
    inf.seek(io::SeekFrom::Start(0))?;
    if (size & 0x1FFFF) != 0 {
        panic!("Block device has to be a multiple of 128KiB in size!");
    }
    let block_count = size / (128 * 1024);
    let mut pb = ProgressBar::new(size);
    pb.set_units(Units::Bytes);
    let mut buf1: [u8; 128 * 1024] = [0; 128 * 1024];
    let mut buf2: [u8; 128 * 1024] = [0; 128 * 1024];
    let mut read_data: u64 = 0;
    let mut written_data: u64 = 0;
    for _ in 0..block_count {
        inf.read(&mut buf1)?;
        outf.read(&mut buf2)?;
        read_data += 128 * 1024;
        if buf1[..] != buf2[..] {
            written_data += 128 * 1024;
            // rewind outfile
            outf.seek(io::SeekFrom::Current(-128 * 1024))?;
            outf.write(&buf1)?;
        }
        pb.add(128 * 1024);
    }
    pb.finish();
    println!("Total Data: {} bytes, Changed Data: {} bytes", read_data, written_data);
    Ok(())
}
